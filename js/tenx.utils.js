/**
 *
 * @type {{debugMode: boolean, attachTerminal: TenX.attachTerminal, print: TenX.print, debug: TenX.debug, screenSize: TenX.screenSize, formatCommand: TenX.formatCommand, generateHelp: TenX.generateHelp}}
 */
var TenX = {

    debugMode: false,

    terminal: undefined,

    /**
     * attach terminal for stdout
     * @param terminal
     */
    attachTerminal: function (terminal) {
        TenX.terminal = terminal;
    },

    /**
     * throw error
     * @param e
     */
    raise: function (e) {
        if (typeof Error !== "undefined") {
            throw new Error(e);
        }
        throw e;
    },

    /**
     * stdout print
     *
     * @param s
     */
    print: function (s) {
        if (TenX.terminal && TenX.terminal.echo) {
            TenX.terminal.echo('[[;crimson;]' + s + ']');
        } else {
            console.log(s);
        }
    },

    /**
     * @return now date logging timestamp
     */
    nowStamp: function () {
        var now = new Date();
        return "{0}/{1}/{2} {3}:{4}:{5} {6}".format(now.getDate(),
            now.getMonth() + 1,
            now.getFullYear(),
            now.getHours(),
            now.getMinutes(),
            now.getSeconds(),
            now.getMilliseconds()
        ).rpad(22, ' ');
    },

    /**
     * debugging purpose
     * @param s
     */
    debug: function (s) {
        if (TenX.debugMode) {
            if (TenX.terminal && TenX.terminal.echo) {
                TenX.terminal.echo('[[;crimson;]' + TenX.nowStamp() + ' > ' + s + ']');
            } else {
                console.log(s);
            }
        }
    },

    /**
     * get the viewPort width, height
     * @returns {{x: (Number|number), y: (Number|number)}}
     */
    screenSize: function () {
        var w = window,
            d = document,
            e = d.documentElement,
            g = d.getElementsByTagName('body')[0],
            x = w.innerWidth || e.clientWidth || g.clientWidth,
            y = w.innerHeight || e.clientHeight || g.clientHeight;
        return {
            width: x, height: y
        };
    },

    /**
     * Command Help Terminal Output Format
     *
     * @param cmd
     * @param desc
     * @returns {string}
     */
    formatCommand: function (cmd, desc) {
        var spaces = '';
        while ((cmd + spaces).length < 25) spaces += ' ';
        return '[[;yellow;]' + cmd + ']\t' + spaces + desc + '\n\t\t\t';
    },

    /**
     * generate terminal output help text
     *
     * @param array [ ['clear', 'clear console'] , [ 'help', 'show help'] ]
     */
    generateHelp: function (array) {
        var helpText = 'Commands: \n\t\t\t';
        for (var cmd in array)
            if (array.hasOwnProperty(cmd))
                helpText += TenX.formatCommand(array[cmd][0], array[cmd][1]);
        return helpText;
    },

    /**
     * prints out the edges
     * @param edges Array of Edge
     */
    printEdges: function (edges) {
        var show = '\n';
        var template = '{0}({1}, {2})\n';
        for (var i in edges) {
            var edge = edges[i];
            var index = '#' + ('' + edge.index).lpad(3, '0');
            var exchange = edge.exchange.rpad(10, ' ');
            var currency = edge.currency.lpad(4, ' ');
            show += template.format('', exchange, currency);
        }
        TenX.print(show);
    },

    /**
     * prints out the vertices
     * @param vertices Array of Vertex
     */
    printVertices: function (vertices) {
        var show = '\n';
        var template = '{0}({1}, {2}) -- {3} --> {4}({5}, {6})\n';
        for (var i in vertices) {
            var vertex = vertices[i];
            var fI = '#' + ('' + vertex.from.index).lpad(3, '0');
            var fX = vertex.from.exchange.rpad(10, ' ');
            var fC = vertex.from.currency.lpad(4, ' ');
            var tI = ('#' + vertex.to.index).lpad(3, '0');
            var tX = vertex.to.exchange.rpad(10, ' ');
            var tC = vertex.to.currency.rpad(4, ' ');
            var w = (vertex.weight + '').lpad(7, ' ');
            show += template.format('', fX, fC, w, '', tX, tC);
        }
        TenX.print(show);
    }
};