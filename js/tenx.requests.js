/**
 * Incoming Exchange Request
 *
 * @param sourceExchange string
 * @param sourceCurrency string
 * @param destinationExchange string
 * @param destinationCurrency string
 * @constructor
 */
function ExchangeRequest(sourceExchange, sourceCurrency, destinationExchange, destinationCurrency) {
    this.sourceExchange = sourceExchange;
    this.sourceCurrency = sourceCurrency;
    this.destinationCurrency = destinationCurrency;
    this.destinationExchange = destinationExchange;
}

ExchangeRequest.prototype.json = function (indent) {
    return indent ? JSON.stringify(this, null, "\t") : JSON.stringify(this);
};

/**
 * Incoming Price Update Request Object
 *
 * @param timeStamp Date
 * @param exchange string
 * @param sourceCurrency string
 * @param destinationCurrency string
 * @param forwardFactor number
 * @param backwardFactor number
 * @constructor
 */
function PriceUpdateRequest(timeStamp, exchange, sourceCurrency, destinationCurrency, forwardFactor, backwardFactor) {
    this.timestamp = timeStamp;
    this.exchange = exchange;
    this.sourceCurrency = sourceCurrency;
    this.destinationCurrency = destinationCurrency;
    this.forwardFactor = forwardFactor;
    this.backwardFactor = backwardFactor;
}

PriceUpdateRequest.prototype.json = function (indent) {
    return indent ? JSON.stringify(this, null, "\t") : JSON.stringify(this);
};