function TenxApp() {

    var me = this;

    var size = TenX.screenSize();

    TenX.debugMode = false;

    var showHelp = function () {
        this.echo(TenX.generateHelp([
            ['clear', 'clears the console'],
            ['help', 'shows available commands'],
            ['dbg', 'switch on/off debug output. off by default'],
            ['test', 'runs the unit tests'],
            ['restart', 'restarts the system'],
            [''.rpad(25, '.'), ''.rpad(80, '.')],
            ['SHOW', "show system data, show options: EDGES, VERTICES\n" + "".lpad(41, ' ') + "usage:[[b;yellow;] SHOW EDGES] or [[b;yellow;]SHOW VERTICES]"],
            ['PRICE_UPDATE', "execute a price update request,\n" + "usage:[[b;yellow;] PRICE_UPDATE '2017-11-01T09:42:23+00:00 KRAKEN BTC USD 1000.0 0.0009']".lpad(130, ' ')],
            ['EXCHANGE_RATE_REQUEST', "execute an exchange rate request,\n" + "usage:[[b;yellow;] EXCHANGE_RATE_REQUEST 'KRAKEN BTC GDAX USD']".lpad(104, ' ')]
        ]));
    };

    var runUnitTests = function () {
        TenXTester.runTests();
    };

    var processPriceUpdate = function (raw) {
        me.engine.updatePrice(raw);
    };

    var processExchangeRequest = function (raw) {
        me.engine.showOptimalPath(raw);
    };

    var switchDebug = function () {
        TenX.debugMode = !TenX.debugMode;
        this.echo('Debug Mode is : ' + TenX.debugMode);
    };

    var processShow = function (cmd) {
        if (cmd === 'edges' || cmd === 'EDGES') {
            me.engine.showEdges();
        } else if (cmd === 'vertices' || cmd === 'VERTICES') {
            me.engine.showVertices();
        } else {
            this.echo('show options: edges, vertices')
        }
    };

    var restart = function () {
        window.location = '.';
    };

    me.terminalCommands = {

        help: showHelp,
        HELP: showHelp,

        test: runUnitTests,
        TEST: runUnitTests,

        dbg: switchDebug,
        DBG: switchDebug,

        restart: restart,
        RESTART: restart,

        SHOW: processShow,
        show: processShow,

        price_update: processPriceUpdate,
        PRICE_UPDATE: processPriceUpdate,

        exchange_rate_request: processExchangeRequest,
        EXCHANGE_RATE_REQUEST: processExchangeRequest
    };

    me.engine = new TenxEngine();

    var greetings =
        '\n'.lpad(58, '*') +
        '* [[b;lightblue;]' + 'TenX Console' + ']' + '*\n'.lpad(44) +
        '* [[;tomato;]Convert & Spend Virtual Currencies. Anytime. Anywhere ]' + '*\n'.lpad(2) +
        ''.rpad(57, '*') + '\n'.rpad(51, '') + '[[b!i;lightblue;]' + 'FadyAro' + ']'
        + '\n\n' +
        '[[b;;]TenX > ]type [[;yellow;]help] for the list of commands.';

    me.terminalOptions = {
        greetings: greetings,
        name: 'tenx',
        height: size.y - 20,
        width: size.x - 20,
        prompt: 'TenX > '
    };
}

TenxApp.prototype.updateTerminalLayout = function () {
    if (this.terminal) {
        var size = TenX.screenSize();
        this.terminal.css('width', size.width - 20);
        this.terminal.css('height', size.height - 20);
    }
};

TenxApp.prototype.start = function () {

    this.terminal = $('#terminal').terminal(this.terminalCommands, this.terminalOptions);

    this.updateTerminalLayout();

    TenX.attachTerminal(this.terminal);

    setTimeout(function () {
        $("a[href]").attr('href', 'http://braingerm.com/fadyaro');
    }, 100);
};