/**
 * TenX Raw String Request Parser
 * @constructor
 */
function TenXParser() {

}

/**
 *
 * Parse ExchangeRequest from raw string
 *
 * @param raw string
 * @returns {ExchangeRequest}
 */
TenXParser.prototype.parseExchangeRequest = function (raw) {

    if (!raw || typeof raw !== 'string')
        throw 'invalid exchange request [' + raw + ']';

    var parts = raw.split(' ');

    // check the 6 chunks
    if (parts.length !== 5 && parts[0] !== 'EXCHANGE_RATE_REQUEST')
        throw 'invalid exchange request [' + raw + ']';

    var data = {
        sourceExchange: parts[1],
        sourceCurrency: parts[2],
        destinationExchange: parts[3],
        destinationCurrency: parts[4]
    };

    // exchanges validation
    if (options.validation.exchanges) {
        if (options.allowed.exchanges.indexOf(data.sourceExchange) < 0)
            throw 'not allowed source exchange: ' + data.sourceExchange;
        if (options.allowed.exchanges.indexOf(data.destinationExchange) < 0)
            throw 'not allowed destination exchange: ' + data.destinationExchange;
    }

    // currencies validation
    if (options.validation.currencies) {
        if (options.allowed.currencies.indexOf(data.sourceCurrency) < 0)
            throw 'not allowed source currency:' + data.sourceCurrency;
        if (options.allowed.currencies.indexOf(data.destinationCurrency) < 0)
            throw 'not allowed destination currency:' + data.destinationCurrency;
    }

    return new ExchangeRequest(
        data.sourceExchange,
        data.sourceCurrency,
        data.destinationExchange,
        data.destinationCurrency
    );
};

/**
 *
 * Parse PriceUpdateRequest from raw string
 *
 * @param raw string
 * @returns {PriceUpdateRequest}
 */
TenXParser.prototype.parsePriceUpdateRequest = function (raw) {

    if (!raw || typeof raw !== 'string')
        throw 'invalid exchange request [' + raw + ']';

    var parts = raw.split(' ');

    // check the 6 chunks
    if (parts.length !== 6)
        throw 'invalid price update request [' + raw + ']';

    var data = {
        timestamp: parts[0],
        exchange: parts[1],
        sourceCurrency: parts[2],
        destinationCurrency: parts[3],
        forwardFactor: parts[4],
        backwardFactor: parts[5]
    };

    // validate iso datetime 8601
    if (!options.regex.isoDate8601.test(data.timestamp))
        throw ('invalid date');
    else data.timestamp = Date.parse(data.timestamp);

    // exchanges validation
    if (options.validation.exchanges && options.allowed.exchanges.indexOf(data.exchange) < 0)
        throw 'not allowed exchange: ' + data.exchange;

    // currencies validation
    if (options.validation.currencies) {
        if (options.allowed.currencies.indexOf(data.sourceCurrency) < 0)
            throw 'not allowed source currency:' + data.sourceCurrency;
        if (options.allowed.currencies.indexOf(data.destinationCurrency) < 0)
            throw 'not allowed destination currency: ' + data.destinationCurrency;
    }

    // validate src / dst currencies
    if (data.sourceCurrency === data.destinationCurrency)
        throw 'source and destination currencies should be different: ' + data.sourceCurrency + ', ' + data.destinationCurrency;

    // validate forward factor
    data.forwardFactor = Number(data.forwardFactor);
    if (!data.forwardFactor || data.forwardFactor <= 0)
        throw 'invalid forward factor: ' + data.forwardFactor;

    // validate backward factor
    data.backwardFactor = Number(data.backwardFactor);
    if (!data.backwardFactor || data.backwardFactor <= 0)
        throw 'invalid backward factor: ' + data.backwardFactor;

    if (data.backwardFactor * data.forwardFactor > 1)
        throw 'invalid weights, backward * forward factor = ' + (data.backwardFactor * data.forwardFactor) + ' > 1';

    if (data.backwardFactor < 0 || data.forwardFactor < 0)
        throw 'invalid backward / forward factor: ' + data.backwardFactor;


    return new PriceUpdateRequest(
        data.timestamp,
        data.exchange,
        data.sourceCurrency,
        data.destinationCurrency,
        data.forwardFactor,
        data.backwardFactor
    );
};