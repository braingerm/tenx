function onReady() {

    var app = new TenxApp();

    $(window).resize(function () {
        app.updateTerminalLayout();
    });

    app.start();
}

$(onReady);