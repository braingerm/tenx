/**
 * Graph Edge Point (Exchange, Currency)
 *
 * @param exchange
 * @param currency
 * @param index
 * @constructor
 */
function Edge(exchange, currency, index) {
    this.exchange = exchange;
    this.currency = currency;
    this.index = index;
}

Edge.prototype.json = function (indent) {
    return indent ? JSON.stringify(this, null, "\t") : JSON.stringify(this);
};

Edge.prototype.equals = function (e) {
    return e.exchange === this.exchange && e.currency === this.currency;
};

/**
 * Graph Vertex
 *
 * @param from Edge
 * @param to Edge
 * @param weight Number
 * @param timestamp Number additional time checkpoint
 * @constructor
 */
function Vertex(from, to, weight, timestamp) {
    this.from = from;
    this.to = to;
    this.weight = weight;
    this.timestamp = timestamp;
}

Vertex.prototype.json = function (indent) {
    return indent ? JSON.stringify(this, null, "\t") : JSON.stringify(this);
};