/**
 * TenX Exchange Engine
 *
 * @constructor
 */
function TenxEngine() {

    /*
     * the graph edges
     */
    this.edges = [];

    /*
     * the graph vertices
     */
    this.vertices = [];

    /*
     * the engine raw to request parser
     */
    this.parser = new TenXParser();
}

/**
 * check if edges is registered in the system
 * @param e
 * @return {boolean}
 */
TenxEngine.prototype.hasEdge = function (e) {
    for (var i = 0; i < this.edges.length; i++)
        if (this.edges[i].equals(e))
            return true;
    return false;
};

/**
 * return the system registered edge
 *
 * @param e Edge
 * @returns {*} the edge or null if not found
 */
TenxEngine.prototype.findEdge = function (e) {
    for (var i = 0; i < this.edges.length; i++)
        if (this.edges[i].equals(e))
            return this.edges[i];
    return null;
};

/**
 * finds a vertex for a registered edges pairs (assigned with indexes)
 *
 * @param fromIndex number Edge.index
 * @param toIndex number Edge.index
 * @return Vertex or null if not found
 */
TenxEngine.prototype.findVertex = function (fromIndex, toIndex) {
    for (var v in this.vertices) {
        if (this.vertices[v].from.index === fromIndex && this.vertices[v].to.index === toIndex) {
            return this.vertices[v];
        }
    }
    return null;
};

/**
 * insert edge if not present before
 * @param e
 */
TenxEngine.prototype.insertIfNewEdge = function (e) {
    var edge = this.findEdge(e);
    if (edge === null) {
        e.index = this.edges.length;
        this.edges.push(e);
        TenX.debug('added new edge: ' + e.json());
    } else {
        e.index = edge.index;
        TenX.debug('found existing edge: ' + e.json());
    }
};

/**
 * insert or update the dual - vertex: <exchange>, from <> to, backward <> forward
 *
 * @param ex Edge
 * @param ey Edge
 * @param wxy number weight from edge x to edge y
 * @param wyx number weight from edge y to edge x
 * @param timestamp
 * @returns {number} 0 if new vertices are create else it is an update operation
 */
TenxEngine.prototype.insertOrUpdateDualVertex = function (ex, ey, wxy, wyx, timestamp) {

    var found = 0;

    /*
     * loop registered vertices to update existing edges new weights
     * dual vertex backward, forward: if already registered found definitely will be to 2
     * when found == 2 stop.
     */
    for (var i = 0; found < 2 && i < this.vertices.length; i++) {
        /*
         * check if vertex is found
         */
        if (ex.equals(this.vertices[i].from) && ey.equals(this.vertices[i].to)) {
            /*
             * increment the found indicator (next vertex found)
             */
            found++;
            /*
             * if the request (timestamp) is newer than existing vertex timestamp update the weight and timestamp
             */
            if (timestamp >= this.vertices[i].timestamp) {
                this.vertices[i].timestamp = timestamp;
                this.vertices[i].weight = wxy;
                TenX.debug('updated existing vertex: ' + this.vertices[i].json());
            } else {
                TenX.debug('no update, vertex is up to date: ' + this.vertices[i].json());
            }
        } else if (ey.equals(this.vertices[i].from) && ex.equals(this.vertices[i].to)) {
            /*
             * increment the found indicator (next vertex found)
             */
            found++;
            /*
             * if the request (timestamp) is newer than existing vertex timestamp update the weight and timestamp
             */
            if (timestamp >= this.vertices[i].timestamp) {
                this.vertices[i].timestamp = timestamp;
                this.vertices[i].weight = wyx;
                TenX.debug('updated existing vertex: ' + this.vertices[i].json());
            } else {
                TenX.debug('no update, vertex is up to date: ' + this.vertices[i].json());
            }
        }
    }

    /*
     * if edges not found => found == 0 ==>
     * new dual vertex [KRAKEN BTC -- 1000  --> KRAKEN USD] <> [KRAKEN USD -- 0.000 --> KRAKEN BTC]
     */
    if (found === 0) {
        var vertex = new Vertex(ex, ey, wxy, timestamp);
        this.vertices.push(vertex);
        TenX.debug('created new vertex: ' + vertex.json());

        vertex = new Vertex(ey, ex, wyx, timestamp);
        this.vertices.push(vertex);
        TenX.debug('created new vertex: ' + vertex.json());
    }

    return found;
};

/**
 * adds the  {two way - 1 weight - mono currency - different exchange } { edge to edge } vertex
 * @param src
 * @param dst
 * @param timestamp
 * @return vertices[]
 */
TenxEngine.prototype.insertIfNewMonoVertices = function (src, dst, timestamp) {
    /*
     * 2.1 note that already registered vertex will no be re-inserted again (e.g. KRAKEN USD -- 1 --> GDAX USD )
     */
    for (var i = 0; i < this.edges.length; i++) {
        var edge = this.edges[i];
        /*
         * it is the vertex source Edge
         */
        if (!this.hasEdge(src) && edge.currency === src.currency && edge.exchange !== src.exchange) {
            // (KRAKEN, BTC) -- 1 --> (GDAX,   BTC)
            var vxy = new Vertex(edge, src, 1, timestamp);
            // (GDAX, BTC)   -- 1 --> (KRAKEN, BTC)
            var vyx = new Vertex(src, edge, 1, timestamp);
            this.vertices.push(vxy);
            TenX.debug('created new vertex: ' + vxy.json());
            this.vertices.push(vyx);
            TenX.debug('created new vertex: ' + vyx.json());
        }
        /*
         * it is the vertex destination Edge
         */
        else if (!this.hasEdge(dst) && edge.currency === dst.currency && edge.exchange !== dst.exchange) {
            var vab = new Vertex(edge, dst, 1, timestamp);
            /* KRAKEN USD -- 1 --> GDAX   USD */
            var vba = new Vertex(dst, edge, 1, timestamp);
            /* GDAX   USD -- 1 --> KRAKEN USD */
            this.vertices.push(vab);
            TenX.debug('created new vertex: ' + vab.json());
            this.vertices.push(vba);
            TenX.debug('created new vertex: ' + vba.json());
        }
    }
};

/**
 * show all available vertices in the system
 */
TenxEngine.prototype.showVertices = function () {
    TenX.printVertices(this.vertices);
};

TenxEngine.prototype.showEdges = function () {
    TenX.printEdges(this.edges);
};

/**
 * update the system graph data
 *
 * @param raw
 */
TenxEngine.prototype.updatePrice = function (raw) {

    /*
     * parse the request
     */
    var request = this.parser.parsePriceUpdateRequest(raw);
    TenX.debug('got price update request ' + request.json());

    /*
     * construct the request edges
     */
    var src = new Edge(request.exchange, request.sourceCurrency);
    var dst = new Edge(request.exchange, request.destinationCurrency);

    /*
     * 1. create or update the [same exchange - different currency] vertices
     */
    var found = this.insertOrUpdateDualVertex(src, dst, request.forwardFactor, request.backwardFactor, request.timestamp);

    /*
     * 2. in case of a new exchange create the [1 weight mono currency - different exchanges] vertices
     */
    if (found === 0) {
        TenX.debug('creating mono currency vertices');
        this.insertIfNewMonoVertices(src, dst, request.timestamp);
    } else {
        TenX.debug('no more vertices to create');
    }

    /*
     * 3. finally save the new edges for next rounds and assign the indexes
     */
    this.insertIfNewEdge(src);
    this.insertIfNewEdge(dst);
};

/**
 * calculate the best rates
 */
TenxEngine.prototype.optimalRates = function () {

    var rate = [];

    var next = [];

    /*
     * initiate arrays
     */
    for (var i = 0; i < this.vertices.length; i++) {
        rate.push([]);
        next.push([]);
        for (var k = 0; k < this.vertices.length; k++) {
            rate[i].push(0);
            next[i].push(null);
        }
    }

    /*
     * implement the optimization
     */
    for (i = 0; i < this.vertices.length; i++) {
        rate[this.vertices[i].from.index][this.vertices[i].to.index] = this.vertices[i].weight;
        next[this.vertices[i].from.index][this.vertices[i].to.index] = this.vertices[i].to.index;
    }

    for (k = 0; k < this.vertices.length; k++) {
        for (i = 0; i < this.vertices.length; i++) {
            for (var j = 0; j < this.vertices.length; j++) {
                if (rate[i][j] < rate[i][k] * rate[k][j]) {
                    rate[i][j] = rate[i][k] * rate[k][j];
                    next[i][j] = next[i][k];
                }
            }
        }
    }

    return next;
};

/**
 * finds the optimal path
 * @param from  Edge
 * @param to    Edge
 * @returns {Array} Edge Indexes
 */
TenxEngine.prototype.optimalPath = function (from, to) {

    /*
     * calculate the optimal rates
     */
    var next = this.optimalRates();

    /*
     * reform the path
     */
    var path = [];
    var u = from.index;
    var v = to.index;
    if (next[u][v] !== null) {
        path.push(u);
        while (u !== v) {
            u = next[u][v];
            path.push(u);
        }
    }

    return path;
};

/**
 * shows the best path rates
 *
 * @param raw
 */
TenxEngine.prototype.showOptimalPath = function (raw) {
    /*
     * parse the request
     */
    var rq = this.parser.parseExchangeRequest('EXCHANGE_RATE_REQUEST ' + raw);

    /*
     * construct the request edges: (exchange, currency) pairs
     */
    var sourceEdge = this.findEdge(new Edge(rq.sourceExchange, rq.sourceCurrency));
    TenX.debug('found source Edge: ' + (sourceEdge ? sourceEdge.json() : null));

    var destinationEdge = this.findEdge(new Edge(rq.destinationExchange, rq.destinationCurrency));
    TenX.debug('found destination Edge: ' + (destinationEdge ? destinationEdge.json() : null));

    /*
     * if edges registered in the system search for the optimal path
     */
    if (sourceEdge !== null && destinationEdge !== null) {
        /*
         * optimal path indexes
         */
        TenX.debug('getting optimal path');
        var pathIndexes = this.optimalPath(sourceEdge, destinationEdge);
        if (pathIndexes.length > 0) {
            var header = 'BEST_RATES_BEGIN {0} {1} {2} {3} {4}\n';
            var row = '{0} {1}\n';
            var footer = 'BEST_RATES_END';

            var rate = 1;
            var rows = '';
            for (var p = 1; p < pathIndexes.length; p++) {
                var fi = pathIndexes[p - 1];
                var ti = pathIndexes[p];
                var vertex = this.findVertex(fi, ti);
                if (vertex === null)
                    throw 'unexpected error, find vertex: {0}, {1} returned null'.format(fi, ti);
                rate *= vertex.weight;
                rows += row.format(vertex.from.exchange, vertex.from.currency);
            }
            rows += row.format(vertex.to.exchange, vertex.to.currency);

            /*
             * stdout
             */
            TenX.print(
                header.format(rq.sourceExchange, rq.sourceCurrency, rq.destinationExchange, rq.destinationCurrency, rate) +
                rows +
                footer
            );
        } else {
            TenX.print('No Path Available For The Moment');
        }
    } else {
        TenX.print('No Path Available For The Moment');
    }
};