/**
 *
 * Unit Test Assert function
 *
 * @param condition
 * @param message
 */
function assert(condition, message) {
    if (!condition) {
        message = message || "Assertion failed";
        if (typeof Error !== "undefined") {
            throw new Error(message);
        }
        throw message;
    }
}

var TenXTester = {

    terminal: null,

    /**
     * parse update price request - unit test
     */
    parse_update_price_request: function () {

        /*
        round 1
         */
        var raw = '2017-11-01T09:42:23+00:00 KRAKEN BTC USD 1000.0 0.0009';

        var request = new TenXParser().parsePriceUpdateRequest(raw);

        assert(request.timestamp === Date.parse('2017-11-01T09:42:23+00:00'), 'wrong timestamp');
        assert(request.exchange === 'KRAKEN', 'wrong exchange');
        assert(request.sourceCurrency === 'BTC', 'wrong source currency');
        assert(request.destinationCurrency === 'USD', 'wrong destination currency');
        assert(request.forwardFactor === Number('1000.0'), 'wrong forward factor');
        assert(request.backwardFactor === Number('0.0009'), 'wrong backward factor');

        /*
        another round
         */
        raw = '2017-11-01T09:42:23+00:00 GDAX BTC USD 1001.0 0.0008';

        request = new TenXParser().parsePriceUpdateRequest(raw);

        assert(request.timestamp === Date.parse('2017-11-01T09:42:23+00:00'), 'wrong timestamp');
        assert(request.exchange === 'GDAX', 'wrong exchange');
        assert(request.sourceCurrency === 'BTC', 'wrong source currency');
        assert(request.destinationCurrency === 'USD', 'wrong destination currency');
        assert(request.forwardFactor === Number('1001.0'), 'wrong forward factor');
        assert(request.backwardFactor === Number('0.0008'), 'wrong backward factor');

        TenX.debug('Test Pass => parse_update_price_request');
    },

    /**
     * parse exchange request - unit test
     */
    parse_exchange_request: function () {

        var raw = 'EXCHANGE_RATE_REQUEST KRAKEN BTC GDAX USD';

        var request = new TenXParser().parseExchangeRequest(raw);

        assert(request.sourceExchange === 'KRAKEN', 'wrong source exchange');
        assert(request.sourceCurrency === 'BTC', 'wrong source currency');
        assert(request.destinationExchange === 'GDAX', 'wrong destination currency');
        assert(request.destinationCurrency === 'USD', 'wrong destination currency');

        TenX.debug('Test Pass => parse_exchange_request');
    },

    /**
     * run all test
     */
    runTests: function () {

        this.parse_update_price_request();

        this.parse_exchange_request();

        TenX.debug('All Tests Passed');
    }
};