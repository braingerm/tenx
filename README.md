**TenX - The Exchange Rate Path**
=
## **Exchange Rate Path Solution Demo.** ##

Accessible Online: [**TenX Web Console**](https://braingerm.com/tenx) 

> **General Commands:**

> - `clear` clears the console
> - `help` shows available commands
> - `dbg` switch on/off debug output. 
> - `test` runs the unit tests

> **Exchange Commands:**

> - `SHOW` shows data, argument: **EDGES** or **VERTICES**
> usage example: `show egdes` 
> - `PRICE_UPDATE` execute a price update request,
> usage example: `PRICE_UPDATE '2017-11-01T09:42:23+00:00 KRAKEN BTC USD 1000.0 0.0009'`
> -  `EXCHANGE_RATE_REQUEST` execute an exchange rate request,
> usage example: `EXCHANGE_RATE_REQUEST 'KRAKEN BTC GDAX USD'`


----------

**Demo**

Start the console

 - Online: go to [**TenX Web Console**](https://braingerm.com/tenx) 
 - Offline: open **index.html** download withing the source code in chrome or another browser.

Let us try this scenario:

 1. Commit 4 price updates from many exchanges/currencies
 2. Check Exchange Data: Edges and Vertices
 3. Commit an Exchange Rate and have the optimal Path
 4. Commit a price update with better rate
 5. Commit and Exchange Rate and validate the optimized Path
 
**from the TenX Console we will execute these 4 prices updates:**

    TenX > PRICE_UPDATE '2017-11-01T09:42:23+00:00 KRAKEN BTC XMR 1 0.9'

    TenX > PRICE_UPDATE '2017-11-01T09:42:23+00:00 GDAX XMR XRP 2 0.48'

    TenX > PRICE_UPDATE '2017-11-01T09:42:23+00:00 BITSTAMP XRP USD 2.0 0.5'

    TenX > PRICE_UPDATE '2017-11-01T09:42:23+00:00 BITSTAMP XMR USD 4.5 0.03'

**You can check the graph system data using these commands**
**show all the edges:**

    TenX > show edges

**stdout:**

    (KRAKEN    ,  BTC)
    (KRAKEN    ,  XMR)
    (GDAX      ,  XMR)
    (GDAX      ,  XRP)
    (BITSTAMP  ,  XRP)
    (BITSTAMP  ,  USD)
    (BITSTAMP  ,  XMR)

**Show all the vertices:**

    TenX > show vertices

   
**stdout:**

    (KRAKEN    ,  BTC) --       1 --> (KRAKEN    , XMR )
    (KRAKEN    ,  XMR) --     0.9 --> (KRAKEN    , BTC )
    (GDAX      ,  XMR) --       2 --> (GDAX      , XRP )
    (GDAX      ,  XRP) --    0.48 --> (GDAX      , XMR )
    (KRAKEN    ,  XMR) --       1 --> (GDAX      , XMR )
    (GDAX      ,  XMR) --       1 --> (KRAKEN    , XMR )
    (BITSTAMP  ,  XRP) --       2 --> (BITSTAMP  , USD )
    (BITSTAMP  ,  USD) --     0.5 --> (BITSTAMP  , XRP )
    (GDAX      ,  XRP) --       1 --> (BITSTAMP  , XRP )
    (BITSTAMP  ,  XRP) --       1 --> (GDAX      , XRP )
    (BITSTAMP  ,  XMR) --     4.5 --> (BITSTAMP  , USD )
    (BITSTAMP  ,  USD) --    0.03 --> (BITSTAMP  , XMR )
    (KRAKEN    ,  XMR) --       1 --> (BITSTAMP  , XMR )
    (BITSTAMP  ,  XMR) --       1 --> (KRAKEN    , XMR )
    (GDAX      ,  XMR) --       1 --> (BITSTAMP  , XMR )
    (BITSTAMP  ,  XMR) --       1 --> (GDAX      , XMR )

**now let us invoke an exchange request**

    TenX > EXCHANGE_RATE_REQUEST 'KRAKEN BTC BITSTAMP XRP' 
**stdout:**

    BEST_RATES_BEGIN KRAKEN BTC BITSTAMP XRP 2.25
    RAKEN BTC
    KRAKEN XMR
    BITSTAMP XMR
    ITSTAMP USD
    BITSTAMP XRP
    BEST_RATES_END

**analysis:** 
the stdout shows the best path with rate 2.5
that means the best path for exchanging Bitcoin to Ripple is by:

 6. Converting Bitcoin to Monero via KRAKEN with rate 1
 7. Jumping from Monero to $ via BITSTAMP with rate 4.5
 8. Finally Jumping from $ to Ripple via BITSTAMP with rate 0.5

the final rate is **1 * 4.5 * 0.5 = 2.5** as indicated, and this seems legit, 
> **e.g** while you can pass from **KRAKEN BTC BITSTAMP XRP** via a lower rate (1 * 2 = 2):
going via KRAKEN, BTC to KRAKEN, XMR with rate 1
finally jumping GDAX, XMR to GDAX, XRP with rate 2

**another price update**
we can turn on debugging to check whats happening

    TenX > dbg
 **stdout:**

     Debug Mode is : true*

 
 Let us increase the GDAX XMR / XRP rate to 4 and check the optimal path calculation: 
 Note that forward*backward should be lower than 1 ( = 4 * 0.18 )

    TenX > PRICE_UPDATE '2017-11-01T09:42:23+00:00 GDAX XMR XRP 4 0.18'
   **stdout:** (debug on)

    17/12/2017 16:6:54 900 > got price update request
    17/12/2017 16:6:54 903 > updated existing vertex: {"from":{"exchange":"GDAX","currency":"XMR","index":2},"to":{"exchange":"GDAX","currency":"XRP","index":3},"weight":4,"timestamp":15095293430
    00}17/12/2017 16:6:54 905 > updated existing vertex: {"from":{"exchange":"GDAX","currency":"XRP","index":3},"to":{"exchange":"GDAX","currency":"XMR","index":2},"weight":0.18,"timestamp":15095293
    43000}
    17/12/2017 16:6:54 909 > no more vertices to create
    17/12/2017 16:6:54 910 > found existing edge: {"exchange":"GDAX","currency":"XMR","index":2}
    17/12/2017 16:6:54 911 > found existing edge: {"exchange":"GDAX","currency":"XRP","index":3}


**now let us recheck the exchange path request**

     TenX > EXCHANGE_RATE_REQUEST 'KRAKEN BTC BITSTAMP XRP'
   **stdout :** (debug on)

    17/12/2017 16:10:11 322 > found source Edge: {"exchange":"KRAKEN","currency":"BTC","index":0}
    17/12/2017 16:10:11 323 > found destination Edge: {"exchange":"BITSTAMP","currency":"XRP","index":4}
    17/12/2017 16:10:11 324 > getting optimal path
    BEST_RATES_BEGIN KRAKEN BTC BITSTAMP XRP 4
    KRAKEN BTC
    KRAKEN XMR
    GDAX XMR
    GDAX XRP
    BITSTAMP XRP
    BEST_RATES_END
   
**analysis:** we can directly tell that the new optimal path changed, and it it true, we have the better rate 4 better than the recent 2.5 one, and the exchange path has switch to: ***KRAKEN > GDAX  > BITSTAMP***

**Demo Screenshots:**

![TenX Web Console ](http://braingerm.com/tenx/images/console_start.png)

![TenX Web Console ](http://braingerm.com/tenx/images/demo.png)

![TenX Web Console ](http://braingerm.com/tenx/images/demo_update.png)

***Thank You!***
***Fady Aro***